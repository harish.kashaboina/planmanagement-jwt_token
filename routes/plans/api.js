const router = require('express').Router();
const Plan = require('../../models/plan');
const Methods = require('../../methods/custom');
const jwt = require('jsonwebtoken');
const config = require('../../config/config');

const apiOperation = (req, res,crudMethod,optionsObj)=> {
	const bodyParams = Methods.initializer(req, Plan);
	crudMethod(bodyParams, optionsObj, (err, plan) => {
		console.log('\nplan details', plan);
		res.send(plan);
	})
}

// Here the roo path '/' is  '/api/plans/'
router.get('/', (req, res) => {
	res.send('This is the api route for plan management');
});

router.get('/getallplans', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					const table = req.query.table;
					Plan.selectTable(table);
					Plan.query('Plan',(err,plans)=>{
						//The error is already handled in the query method; but the error first approach is used here, so if we wan't to do the error handling
						//here instead of the query function, it will be easier to switch back
						res.send(plans);
					})
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

router.post('/getplan', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					apiOperation(req, res, Plan.getItem);
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

router.post('/addplan', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
				const bodyParams = Methods.initializer(req, Plan);
				Plan.createItem(bodyParams, {
					table: decode.storeName+"_store_data",
					overwrite: false
				}, (err, plan) => {
					console.log('\nplan details', plan);
					res.send(plan);
				})
			}
        })
    }
    else{
        res.send("please send a token");
    }
});

router.put('/updateplan', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
				apiOperation(req, res, Plan.updateItem);
				   
            }
        })
    }
    else{
        res.send("please send a token");
    }
});

router.delete('/deleteplan', (req, res) => {
	var token = req.body.token || req.headers['token'];	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					apiOperation(req, res, Plan.deleteItem);
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

module.exports = router;